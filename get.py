from jira import JIRA

jiraOptions = {'server': 'https://your-domain.atlassian.net'}
jira = JIRA(options=jiraOptions, basic_auth=('your-email@example.com', 'your-api-token'))

for singleIssue in jira.search_issues('project = YourProjectKey'):
    print(' {}: {}: {}'.format(singleIssue.key, singleIssue.fields.summary, singleIssue.fields.reporter.displayName))
